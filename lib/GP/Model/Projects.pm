package GP::Model::Projects;
use strict;
use warnings;
use JSON;
use GP::Model::Project;
use parent 'GP::Model::Database';

sub new {
    my $class = shift;
    return $class->SUPER::new('projects.db');
}

sub get {
    my ($self, $key) = @_;
    if (my $val = $self->SUPER::get($key)) {
	return new GP::Model::Project($key,$val);
    }
}

sub values {
    my ($self) = @_;
    map { $self->get($_) } $self->keys;
}

1;
