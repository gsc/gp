package GP::Model::Project;
use strict;
use warnings;
use JSON;
use Carp;
use GP::DateTime;

sub new {
    my ($class, $name, $val) = @_;
    my $self = bless decode_json($val), $class;
    if ($self->{modified_date}) {
	$self->{modified_date} = new GP::DateTime($self->{modified_date});
    }
    if ($self->{start_date}) {
	$self->{start_date} = new GP::DateTime($self->{start_date});
    }
    $self->{name} = $name;
    $self;
}

sub category { @{shift->{category} // [] } }

sub in_category {
    my $self = shift;
    unless (exists($self->{_catmap})) {
	@{$self->{_catmap}}{$self->category} = (1) x $self->category;
    }
    foreach my $arg (@_) {
	return 1 if ($self->{_catmap}{$arg});
    }
}
	       
sub related { @{shift->{related} // [] } }

sub in_related {
    my $self = shift;
    unless (exists($self->{_relmap})) {
	@{$self->{_relmap}}{$self->related} = (1) x $self->related;
    }
    foreach my $arg (@_) {
	return 1 if ($self->{_relmap}{$arg});
    }
}
    

my @ATTRIBUTES = qw(name news_url repository description start_date modified_date news url version news_format);

{
    no strict 'refs';
    foreach my $attribute (@ATTRIBUTES) {
        *{ __PACKAGE__ . '::' . $attribute } = sub {
            my $self = shift;
            if (defined(my $val = shift)) {
                croak "too many arguments" if @_;
                $self->{$attribute} = $val;
            }
            return $self->{$attribute};
        }
    }
}

1;
