package GP::Model::Categories;
use strict;
use warnings;
use GP::Model::Category;
use parent 'GP::Model::Database';

sub new {
    my $class = shift;
    return $class->SUPER::new('categories.db');
}

sub get {
    my ($self, $key) = @_;
    if (my $val = $self->SUPER::get($key)) {
	return new GP::Model::Category($key,$val);
    }
}

sub values {
    my ($self) = @_;
    map { $self->get($_) } $self->keys;
}

1;
