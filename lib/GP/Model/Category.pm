package GP::Model::Category;
use strict;
use warnings;
use JSON;
use GP::Model::Projects;

sub new {
    my ($class, $name, $val) = @_;
    my $self = bless decode_json($val), $class;
    $self->{name} = $name;
    $self;
}

sub name { shift->{name} }
sub description { shift->{description} }
sub summary { shift->{summary} }

sub projects {
    my ($self) = @_;
    grep { $_->in_category($self->name) } GP::Model::Projects->new->values;
}

use overload
    '""' => sub { shift->name };

1;
