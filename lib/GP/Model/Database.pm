package GP::Model::Database;
use strict;
use warnings;
use GDBM_File;
use JSON;
use File::Basename;
use File::Spec;

sub new {
    my ($class, $dbname) = @_;

    (my $file = $class) =~ s{::}{/}g;
    $file .= '.pm';

    my @d = split '/', dirname($INC{$file});
    my $file_name = File::Spec->catfile(@d[0 .. $#d-3], 'data', $dbname);

    my %h;
    tie %h, 'GDBM_File', $file_name, &GDBM_READER, 0640;

    my $json = JSON->new->allow_nonref;
    return bless { href => \%h, filename => $file_name }, $class;
}

sub has {
    my ($self, $key) = @_;
    return exists($self->{href}{$key});
}

sub get {
    my ($self, $key) = @_;
    return $self->{href}{$key};
}

sub keys {
    my ($self) = @_;
    return keys %{$self->{href}}    
}

sub values {
    my ($self) = @_;
    return values %{$self->{href}}
}

1;

