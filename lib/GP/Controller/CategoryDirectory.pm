package GP::Controller::CategoryDirectory;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use GP::Model::Categories;

sub list {
    my $self = shift;
    $self->stash(categories => GP::Model::Categories->new);
    $self->render(template => 'categories/list');
}    

1;
