package GP::Controller::ProjectDirectory;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use GP::Model::Projects;

sub directory {
    my $self = shift;
    $self->stash(projects => GP::Model::Projects->new);
    $self->render(template => 'projects/directory');
}

sub list {
    my $self = shift;
    $self->stash(projects => GP::Model::Projects->new);
    my $category;
    if (my $catname = $self->param('C')) {
	$category = GP::Model::Categories->new->get($catname);
    }
    $self->stash(category => $category);
    
    $self->render(template => 'projects/list');
}    

sub project_list {
    my $self = shift;
    my %sorts = (
	n => sub {
	    my ($a, $b) = @_;
	    $a->name cmp $b->name
	},
	d => sub {
	    my ($a, $b) = @_;
	    if (!defined($a->modified_date)) {
		return defined($b->modified_date) ? -1 : 0
	    } elsif (!defined($b->modified_date)) {
		return defined($a->modified_date) ? 1 : 0
	    }
	    $a->modified_date <=> $b->modified_date
	}
    );

    my $s = $sorts{$self->param('S') // 'n'} // $sorts{n};
    my $sortfn;
    if (($self->param('O') // 'a') eq 'd') {
	$sortfn = sub { - &{$s} }
    } else {
	$sortfn = $s;
    }

    my $category = $self->stash('category');
    sort { &{$sortfn}($a, $b) }
      grep { !$category || $_->in_category($category->name) }
        $self->stash('projects')->values
}

1;
