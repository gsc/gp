package GP::DateTime;
use strict;
use warnings;
use parent 'Time::Piece';
use Carp;

sub new {
    my ($class, $str) = @_;
    my @formats = (
	'%Y-%m-%d',
	'%a %b %d %H:%M:%S %Y',
	'%Y/%m/%d',
	'%Y/%d/%m',
	'%d %B %Y',
	'%d %b %Y',
    );

    unless ($str) {
	return $class->SUPER::new(0);
    }
    
    my @alt = grep { defined $_ } map {
	eval { Time::Piece->strptime($str, $_) }
    } @formats;
    croak "Unparsable date format: $str" unless @alt;
    my $self = bless shift @alt, $class;
    carp "Ambiguous date $str: parsed as $self" if @alt;
    $self
}

sub as_str {
    my ($self) = @_;
    $self->strftime('%Y-%m-%d')
};

use overload
    '""' => \&as_str;

1;

    
