package ReleaseLog::Description;
use strict;
use warnings;

sub new {
    my $class = shift;
    my $self = bless { text => [] }, $class;
    $self->append(@_);
    return $self;
}

sub append {
    my $self = shift;
    push @{$self->{text}},
	map {
	    chomp(my $s = $_);
	    $s =~ s/^\s+//;
	    $s
        } @_;
}

sub lines {
    my ($self) = @_;
    @{$self->{text}}
}

sub as_str {
    my ($self) = shift;
    join("\n", $self->lines);
}

use overload
    '""' => \&as_str;

1;


    
