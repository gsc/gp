package ReleaseLog::History;
use strict;
use warnings;
use Carp;

sub new {
    my $class = shift;
    my $self = bless { releases => [], sorted => 1 }, $class;
    foreach my $rel (@_) {
	$self->add($rel);
    }
    return $self;
}

sub add {
    my ($self, $release) = @_;
    croak "argument must be a ReleaseLog::Release object"
	unless $release->isa('ReleaseLog::Release');
    push @{$self->{releases}}, $release;
    $self->{sorted} = 0;
}

sub sort {
    my ($self) = @_;
    unless ($self->{sorted}) {
	$self->{releases} = [ sort { $b <=> $a } @{$self->{releases}} ];
	$self->{sorted} = 1;
    }
    return @{$self->{releases}};
}

sub get {
    my ($self, $n) = @_;
    return if $n >= @{$self->{releases}};
    return ($self->sort)[$n];
}

sub count {
    my ($self) = @_;
    return $self->sort;
}

sub latest {
    my ($self) = @_;
    return unless $self->count;
    return ($self->sort)[0];
}

sub oldest {
    my ($self) = @_;
    return unless $self->count;
    return ($self->sort)[-1];
}

1;
