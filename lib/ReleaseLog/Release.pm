package ReleaseLog::Release;
use strict;
use warnings;
use GP::DateTime;
use ReleaseLog::Description;
use Carp;

sub new {
    my $class = shift;
    local %_ = @_;

    my $on_error = delete $_{on_error} // 'croak';
    my $error;
    
    my $date = delete $_{date};    
    if ($date) {
	if ($on_error eq 'croak') {
	    $date = GP::DateTime->new($date);
	} else {
	    $date = eval { GP::DateTime->new($date) };
	    if ($@) {
		if ($on_error eq 'store') {
		    chomp($error = $@);
		} elsif ($on_error ne 'ignore') {
		    warn "unrecognized value for on_error: $on_error";
		}
		$date = GP::DateTime->new;
	    }
	}
    }
    my @descr;
    if (my $d = delete $_{description}) {
	if (ref($d) eq 'ARRAY') {
	    @descr = @$d;
	} else {
	    push @descr, $d;
	}
    }
    my $self = bless {	
	date => $date,
	version => delete $_{version},
	description => new ReleaseLog::Description(@descr),
	error => $error    
    }, $class;
    croak "unrecognized arguments" if %_;
    $self;
}

sub error { shift->{error} }
sub ok { ! shift->error }

sub version {
    my $self = shift;
    if (defined(my $arg = shift)) {
	croak "too many arguments" if @_;
	$self->{version} = $arg;
    }
    return $self->{version};
}

sub date {
    my $self = shift;
    if (defined(my $arg = shift)) {
	croak "too many arguments" if @_;
	$self->{date} = GP::DateTime->new($arg);
    }
    return $self->{date};
}

sub description {
    my $self = shift;
    if (@_) {
	$self->{description} = new ReleaseLog::Description(@_);
    }
    return $self->{description};
}

use overload
    '==' => sub {
	       my ($a, $b) = @_;
	       return $a->date == $b->date;
           },
    '>' => sub {
	       my ($a, $b) = @_;
	       return $a->date > $b->date;
           },
    '<=>' => sub {
	       my ($a, $b) = @_;
	       return $a->date <=> $b->date
           },
    'cmp' => sub {
	       my ($a, $b) = @_;
	       return $a->date <=> $b->date
           },
    'bool' => sub { 1 };
	       
1;
    
