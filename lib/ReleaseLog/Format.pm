package ReleaseLog::Format;
use strict;
use warnings;
use parent 'ReleaseLog::History';
use Carp;

sub new {
    my $class = shift;
    my $self = $class->SUPER::new;
    return $self->parse(@_);
}

sub format {
    my $self = shift;
    (my $name = ref($self)) =~ s/^ReleaseLog::Format:://;
    return $name;
}

sub parse_file {
    my ($self, $fh) = @_;
    local %_ = @_;

    my $release;
    
    while (<$fh>) {
	chomp;

	if (my $newrel = $self->parse_header($_)) {
	    last if defined($_{max_count}) && $self->count == $_{max_count};
	    # FIXME: add locus to the $release.
	    $release = $newrel;
	    $self->add($release);
	} elsif ($release) {
	    $release->description->append($_);
	}
    }
}

sub parse {
    my $self = shift;
    local %_ = @_;

    if (my $file = delete $_{file}) {
	open(my $fh, '<', $file) or
	    croak "can't open $file for reading: $!";
	$self->parse_file($fh, name => $file, max_count => delete($_{max_count}));
	close $fh;
    } elsif (my $text = delete $_{text}) {
	open my $fh, "<", \$text;
	$self->parse_file($fh, max_count => delete($_{max_count}));
	close $fh;
    }
    return $self;
}
    
1;

