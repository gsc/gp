package ReleaseLog::Format::GNU;
use strict;
use warnings;
use parent 'ReleaseLog::Format';
use ReleaseLog::Release;

sub filename { return "NEWS" }

sub parse_header {
    my ($self, $str) = @_;
    if ($str =~ m{
	^(?:\*\s+)?    # optional initial section
         (?:(?i)version)\s+
         (?P<version>\d(?:[.,]\d+){1,2}  # At least MAJOR.MINOR
         (?:[\d._-])*)    # Optional patchlevel
         (?:.*[-,:]\s+(?P<date>.+))
	}x) {
	return new ReleaseLog::Release(version => $+{version},
		                       date => $+{date},
	                               on_error => 'store');
    }
}

1;

	    
