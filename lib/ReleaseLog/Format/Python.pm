package ReleaseLog::Format::Python;
use strict;
use warnings;
use parent 'ReleaseLog::Format';
use ReleaseLog::Release;

sub filename { return 'CHANGES.txt' }

sub parse_header {
    my ($self, $str) = @_;
    if ($str =~ m{
        ^[vV]?(?P<version>\d[\d.]*)\s*
        ,\s*
        (?P<date>.*?)
        \s+-+\s*
        (?P<descr>.*)$
	}x) {
	return new ReleaseLog::Release(version => $+{version},
			               date => $+{date},
	                               description => $+{descr},
	                               on_error => 'store');
    } elsif ($str =~ m{
	     ^[Vv]?(?P<version>\d[\d.]*)
             \s*
             \((?P<date>.*)\)$
	     }x) {
	return new ReleaseLog::Release(version => $+{version},
			               date => $+{date},
	                               on_error => 'store');
    } elsif ($str =~ m{
	     ^[vV]?(?P<version>\d[\d.]*)
             \s*
             (?P<date>.*)$
	     }x) {
	return new ReleaseLog::Release(version => $+{version},
			               date => $+{date},
 	                               on_error => 'store');
    }
}

1;

	    
