package ReleaseLog::Format::CPAN;
use strict;
use warnings;
use parent 'ReleaseLog::Format';
use ReleaseLog::Release;

sub filename { return 'Changes' }

sub parse_header {
    my ($self, $str) = @_;
    if ($str =~ m{^(?P<version>\d[\d.]*)\s+(?P<date>.+?)\s*$}) {
	return new ReleaseLog::Release(version => $+{version},
			               date => $+{date},
	                               on_error => 'store');
    }
}

1;

	
    
