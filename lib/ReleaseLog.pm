package ReleaseLog;
use strict;
use warnings;
use Carp;

sub new {
    my ($class, $format, @args) = @_;
    croak 'format not supplied' unless $format;
    my $modname = __PACKAGE__ . '::Format::' . $format;
    my $modpath = $modname;
    $modpath =~ s{::}{/}g;
    $modpath .= '.pm';
    my $obj = eval { require $modpath; $modname->new(@args) };
    if ($@) {
        if ($@ =~ /Can't locate $modpath/) {
            die "unknown format: $format"
        }
        croak $@;
    }
    return $obj;
}

1;


    
    

