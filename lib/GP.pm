package GP;
use Mojo::Base 'Mojolicious', -signatures;

# This method will run once at server start
sub startup ($self) {

  # Load configuration from config file
  my $config = $self->plugin('NotYAMLConfig');

  # Configure the application
  $self->secrets($config->{secrets});

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('Example#welcome');
  $r->get('/dir')->to('ProjectDirectory#directory');
  $r->get('/projects')->to('ProjectDirectory#list');

  $r->get('/categories')->to('CategoryDirectory#list');
}

1;
