# -*- perl -*-
use strict;
use lib qw(t lib);
use Test::More;

BEGIN {
    plan tests => 3;
    use_ok('ReleaseLog::Description');
}

my $rel = new_ok 'ReleaseLog::Description' => ['line 1','line 2'];
is("$rel", "line 1\nline 2");

             

