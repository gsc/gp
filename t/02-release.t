# -*- perl -*-
use strict;
use lib qw(t lib);
use Test::More;

BEGIN {
    plan tests => 10;
    use_ok('ReleaseLog::Release');
}

my $rel = new_ok 'ReleaseLog::Release' =>
                           [ version => '1.0',
			     date => '2017-03-01',
 	                     description => 'some descriptive text' ];
is($rel->ok, 1, 'the ok method');

ok($rel->version, '1.0');
ok($rel->date, '2017-03-01');
ok($rel->description, 'some descriptive text');

my $rel2 = new_ok 'ReleaseLog::Release' =>
                            [ version => '2.0',
			      date => '2017-04-01',
			      description => 'another descriptive text' ];
cmp_ok($rel2, '>', $rel, 'release 2 older than 1');

# Invalid date
$rel = new_ok 'ReleaseLog::Release' =>
                  [ date => 'invalid',
                    on_error => 'store' ];
isnt($rel->ok, 1, 'invalid date: not ok');
