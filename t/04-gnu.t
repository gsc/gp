# -*- perl -*-
use strict;
use lib qw(t lib);
use Test::More;

BEGIN {
    plan tests => 8;
    use_ok('ReleaseLog');
}

my $text = <<\EOT;
Mailfromd NEWS -- history of user-visible changes. 2022-01-02
See the end of file for copying conditions.

Please send Mailfromd bug reports to <bug-mailfromd@gnu.org.ua>
^L
Version 8.13, 2022-01-02

* Fix compilation with mailutils 3.14

* sed

** fix replacement of the requested Nth pattern instance

This fixes sed expressions like s/X/@/2.

^L
Version 8.12 - Sergey Poznyakoff, 2021-08-06

* Filters and filter pipes

A "filter" is ...

* Bugfixes

* version 4.2, 2014-05-23

Something else.    

EOT
;    

my $rlog = ReleaseLog->new('GNU', text => $text);
isa_ok($rlog, 'ReleaseLog::Format::GNU');
is($rlog->count, 3);

is($rlog->get(0)->version, '8.13');
is($rlog->get(1)->version, '8.12');
is($rlog->get(2)->version, '4.2');

is($rlog->latest->version, '8.13');
is($rlog->latest->date->as_str, '2022-01-02');



