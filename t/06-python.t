# -*- perl -*-
use strict;
use lib qw(t lib);
use Test::More;

BEGIN {
    plan tests => 11;
    use_ok('ReleaseLog');
}

my $text = <<\EOT;
Revision history for some Python module.

v1.0.2, 2018-11-05 -- Convert byte-like objects to UTF-8 strings.
    - Some bug fixes.
1.0.1, 2018-10-08 -- Initial release.

0.0.1 (23 June 2018)
    Not released, actually.
EOT
    
;    

my $rlog = ReleaseLog->new('Python', text => $text);
isa_ok($rlog, 'ReleaseLog::Format::Python');
is($rlog->count, 3);

is($rlog->get(0)->version, '1.0.2');
is($rlog->get(0)->description->as_str,
   "Convert byte-like objects to UTF-8 strings.\n- Some bug fixes.");

is($rlog->get(1)->version, '1.0.1');
is($rlog->get(1)->description->as_str, "Initial release.\n");

is($rlog->get(2)->version, '0.0.1');
is($rlog->get(2)->description->as_str, "Not released, actually.");
   
is($rlog->latest->version, '1.0.2');
is($rlog->latest->date->as_str, '2018-11-05');



