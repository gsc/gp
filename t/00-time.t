# -*- perl -*-
use strict;
use lib qw(t lib);
use Test;
use GP::DateTime;

plan tests => 13;

ok(GP::DateTime->new->as_str, '1970-01-01');

ok(GP::DateTime->new('2020-02-03')->as_str, '2020-02-03');
ok(GP::DateTime->new('2020-02-03').'', '2020-02-03');
#ok(GP::DateTime->new('2020/02/03').'', '2020-02-03');
ok(GP::DateTime->new('Sat Jul  7 19:11:35 2018')->as_str, '2018-07-07');
ok(GP::DateTime->new('2014/12/14')->as_str, '2014-12-14');
ok(GP::DateTime->new('23 June 2018')->as_str, '2018-06-23');

ok(GP::DateTime->new('23 June 2018') == GP::DateTime->new('2018-06-23'), 1);
ok(GP::DateTime->new('23 June 2018') >= GP::DateTime->new('2018-06-23'), 1);
ok(GP::DateTime->new('23 June 2018') <= GP::DateTime->new('2018-06-23'), 1);

ok(GP::DateTime->new('24 June 2018') > GP::DateTime->new('2018-06-23'), 1);
ok(GP::DateTime->new('24 June 2018') >= GP::DateTime->new('2018-06-23'), 1);

ok(GP::DateTime->new('21 Jan 2017') < GP::DateTime->new('2018-06-23'), 1);
ok(GP::DateTime->new('21 Jan 2017') <= GP::DateTime->new('2018-06-23'), 1);


    
    
