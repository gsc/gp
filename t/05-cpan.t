# -*- perl -*-
use strict;
use lib qw(t lib);
use Test::More;

BEGIN {
    plan tests => 8;
    use_ok('ReleaseLog');
}

my $text = <<\EOT;
Revision history for Perl extension Config::AST

1.07  Fri Feb 12 20:31:53 2021
        - Change bugtracker address.

1.06  Tue Nov 19 20:37:56 2019
        - Fix method invocation at the end of direct access chain.

1.05  Wed Aug 28 19:27:26 2019
        - implemented case-insensitive keywords
        - new method: describe_keyword
        - provide overloads for direct addressing objects    
EOT
;    

my $rlog = ReleaseLog->new('CPAN', text => $text);
isa_ok($rlog, 'ReleaseLog::Format::CPAN');
is($rlog->count, 3);

is($rlog->get(0)->version, '1.07');
is($rlog->get(1)->version, '1.06');
is($rlog->get(2)->version, '1.05');

is($rlog->latest->version, '1.07');
is($rlog->latest->date->as_str, '2021-02-12');



