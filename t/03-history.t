# -*- perl -*-
use strict;
use lib qw(t lib);
use Test::More;

BEGIN {
    plan tests => 10;
    use_ok(' ReleaseLog::History');
    use_ok('ReleaseLog::Release');
}

my $hist = new_ok 'ReleaseLog::History';

my $rel = new_ok 'ReleaseLog::Release' =>
                     [ version => '1.0',
 	               date => '2017-03-01',
   		       description => 'Version 1.0. Some descriptive text.' ];
$hist->add($rel);


$rel = new_ok 'ReleaseLog::Release' =>
                     [ version => '0.1',
 	               date => '2017-01-10',
   		       description => 'Version 0.1.  Initial version.' ];
$hist->add($rel);

$rel = new_ok 'ReleaseLog::Release' =>
                     [ version => '2.0',
 	               date => '2017-05-04',
   		       description => 'Version 2.0. Some changes.' ];
$hist->add($rel);

is($hist->count, 3, 'Release count');

is($hist->get(0)->version, '2.0', 'Entry 0');
is($hist->get(1)->version, '1.0', 'Entry 1');
is($hist->get(2)->version, '0.1', 'Entry 2');

